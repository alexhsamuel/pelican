Title: I.T.-Vokabeln
Date: 2023-01-25
Category: german
Tags: dev
Summary: Übersetzung von technologischen Fachbegriffen ins Deutsche

Ich bin beruflich Programmierer und als Hobby lerne ich Deutsch.  Ich möchte
über Technologie auf Deutsch reden können.  Das soll einfach sein, schließlich
werden doch viele englischen Wörter im Technologie-Bereich auf Deutsch benutzt,
oder?  Oder auch nicht einfach, weil man wissen muss, _welche_ englishen Wörter
benutzt werden und wie man sie ausspricht (wie auf English, oder mit deutscher
Aussprache?), und dazu für Nomen die Artikeln und Pluralformen, für Verben ob
sie trennbar sind und wie man sie konjugiert.

Als Übung teile ich heir unten eine Liste von Technologievokabeln, sowohl
englische Wörter die auf Deutsch benutzt werden als auch nicht-Lehnwörter, die
sich auf technologische Begriffe beziehen.

Hinweise:
- Pluralformen von Nomen in Klammern.
- Irregulärformen von Verben in Klammern, ansonsten sind sie regulär.


## Allgemeine Begriffe

| englisch   | deutsch                           | Erläuterung                                      |
|:-----------|:----------------------------------|:-------------------------------------------------|
| I.T.       | I.T.                              | _I_ wie auf Englisch ausgesprochen               |
| technology | die Technologie (–n)              |                                                  |
| hardware   | die Hardware                      | _w_ wie auf Englisch ausgesprochen               |
| software   | die Software                      | _w_ wie auf Englisch ausgesprochen               |
| computer   | der Computer (–), der Rechner (–) | _u_ in _Computer_ wie auf Englisch ausgesprochen |


## Hardware

| englisch           | deutsch                                         | Erläuterung                                     |
|:-------------------|:------------------------------------------------|:------------------------------------------------|
| desktop computer   | der Desktop-Computer, der Bürocomputer          |                                                 |
| laptop             | der Laptop (–s)                                 |                                                 |
| monitor            | der Monitor (–e oder -en), der Bildschirm (–e)  |                                                 |
| mouse              | die Maus (–̈e)                                   |                                                 |
| mouse button       | die Maustaste (–n)                              |                                                 |
| mouse pad          | das Mousepad (–s)                               |                                                 |
| trackpad           | das Trackpad (–s)                               |                                                 |
| keyboard           | die Tastatur (–en), das Keyboard                | _das Keyboard_ meistens für das Musikinstrument |
| hard drive         | die Festplatte (–n)                             |                                                 |
| CD drive           | das CD-Laufwerk (–e)                            |                                                 |
| DVD drive          | das DVD-Laufwerk (–e)                           |                                                 |
| memory             | der Speicher, der Speicherplatz                 |                                                 |
| capacity           | die Kapazität                                   |                                                 |
| CPU, processor     | die CPU (–s), der Prozessor (–en)               |                                                 |
| frequency          | die Frequenz (–en), die Taktfrequenz            |                                                 |
| graphics card      | die Graphik-Karte (–n)                          |                                                 |
| plug               | der Stecker (–)                                 |                                                 |
| socket, jack       | die Steckdose (–n), die Buchse (–n)             |                                                 |
| connector          | der Anschluss (–̈e)                              |                                                 |
| to plug in         | einstecken                                      |                                                 |
| to unplug          | ausstecken                                      | oder _den Stecker (heraus)ziehen_               |
| to connect         | anschließen (schloss an, angeschlossen)         |                                                 |
| to disconnect      | trennen                                         |                                                 |
| adapter            | der Adapter (–), der Zwischenstecker (–)        |                                                 |
| button             | die Taste (–n), der Button (–s), der Knopf (–̈e) |                                                 |
| power button       | der An-/Ausschalter (pl —)                      |                                                 |
| power light        | die Lampe                                       |                                                 |
| to turn on         | einschalten, anmachen                           |                                                 |
| to turn off        | ausschalten, abschalten                         |                                                 |
| to restart, reboot | neustarten                                      |                                                 |
| power supply       | das Netzteil (–e)                               |                                                 |
| battery            | der Akku (–s), die Batterie (–n)                |                                                 |
| to recharge        | aufladen (lädt auf, lud auf, hat aufgeladen)    |                                                 |
| wireless           | drahtlos                                        |                                                 |
| wired              | verkabelt, verdrahtet                           | _verdrahtet_ eher fachlich                      |
| fan                | der Lüfter (–)                                  |                                                 |
| webcam             | die Webcam (–s)                                 |                                                 |
| speaker            | der Lautsprecher (–)                            |                                                 |
| housing, case      | das Gehäuse (–n)                                |                                                 |
| upgrade            | das Upgrade                                     |                                                 |
| to upgrade         | (sich) aufwerten, (sich) upgraden               |                                                 |
| internal           | intern                                          |                                                 |
| external           | extern                                          |                                                 |


## Sonstig

| englisch          | deutsch                          | Erläuterung |
|:------------------|:---------------------------------|:------------|
| spell checker     | die Rechtschreibprüfung (pl –en) |             |
| scalability       | die Skalierbarkeit               |             |
| power consumption | die Leistungsaufnahme            |             |
| execution         | dir Ausführung                   |             |
| interface         | die Schnittstelle (pl —n)        |             |
| to update         | updated (upgedated)              |             |
