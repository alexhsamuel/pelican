Title: 5e-Vokabeln
Date: 2023-02-12
Category: german rpg
Tags: dev
Summary: Übersetzung der D&D-Vokabeln ins Deutsche

Diese Listen enthalten Übersetzungen der englischen Fachwörter der fünften
Edition („5e”) des Dungeons & Dragons.

## Classen

Die Pluralform des Nomens, wenn nicht anders angegeben:

- mit –er ist –
- mit –e ist –n
- mit –in ist –nen

| englisch  | deutsch — männlich       | deutsch – weiblich | Erläuterung                  |
|:----------|:-------------------------|:-------------------|:-----------------------------|
| artificer | der Magieschmied (pl –e) | (?)                | oder der Aritifizient (pl ?) |
| barbarian | der Barbar (pl –en)      | die Barbarin       |                              |
| bard      | der Barde                | die Bardin         |                              |
| cleric    | der Kleriker             | die Klerikerin     |                              |
| druid     | der Druide               | die Druidin        |                              |
| fighter   | der Kämpfer              | die Kämpferin      |                              |
| monk      | der Mönch (pl –e)        | die Mönchin (?)    |                              |
| paladin   | der Paladin (pl –e)      | die Paladinin      |                              |
| ranger    | der Waldläufer           | die Waldläuferin   |                              |
| rogue     | der Shurke               | die Shurkin        |                              |
| sorcerer  | der Zauberer             | die Zaubererin     |                              |
| warlock   | der Hexenmeister         | die Hexenmeisterin |                              |
| wizard    | der Magier               | die Magierin       |                              |


### Archetypen

Offiziel sind sie _Archetypen_ bezeichnet, aber sind gewöhnlich als
_Unterklassen ("subclasses")_ benannt.

| englisch                       | deutsch                      | Erläuterung |
|:-------------------------------|:-----------------------------|:------------|
| Alchemist                      | Alchemist                    |             |
| Armorer                        | Rüstungsschmied              |             |
| Artillerist                    | Artillerist                  |             |
| Battle Smith                   | Kampfschmied                 |             |
|                                |                              |             |
| Path of Wild Magic             | Pfad der Wilden Magie        |             |
| Path of the Ancestral Guardian | Pfad des Ahnenwächters       |             |
| Path of the Battlerager        | Pfad des Schlachtenwüters    |             |
| Path of the Beast              | Pfad der Bestie              |             |
| Path of the Storm Herald       | Pfad des Sturmherolds        |             |
| Path of the Totem Warrior      | Pfad des Totemkriegers       |             |
| Path of the Zealot             | Pfad des Zeloten             |             |
|                                |                              |             |
| College of Creation            | Schule der Schöpfung         |             |
| College of Eloquence           | Schule der Eloquenz          |             |
| College of Glamour             | Schule des Zauberbanns       |             |
| College of Lore                | Schule des Wissens           |             |
| College of Spirits             | Schule der Geister           |             |
| College of Swords              | Schule der Schwerter         |             |
| College of Valor               | Schule des Wagemuts          |             |
| College of Whispers            | Schule des Flüsterns         |             |
|                                |                              |             |
| Arcana Domain                  | Arkane-Domäne                |             |
| Death Domain                   | Domäne Tod                   |             |
| Forge Domain                   | Domäne der Schmiede          |             |
| Knowledge Doman                | Domäne des Wissens           |             |
| Life Domain                    | Domäne des Lebens            |             |
| Light Domain                   | Domäne des Lichts            |             |
| Nature Domain                  | Domäne der Natur             |             |
| Order Domain                   | Domäne der Ordnung           |             |
| Peace Domain                   | Domäne des Friedens          |             |
| Tempest Domain                 | Domäne des Sturms            |             |
| Trickery Domain                | Domäne der List              |             |
| Twilight Domain                | Domäne der Dämmerung         |             |
| War Domain                     | Domäne des Krieges           |             |
|                                |                              |             |
| Circle of Dreams               | Zirkel der Träume            |             |
| Circle of Spores               | Zirkel der Sporen            |             |
| Circle of Stars                | Zirkel der Sterne            |             |
| Circle of Wildfire             | Zirkel des Wildfeuers        |             |
| Circle of the Land             | Zirkel des Landes            |             |
| Circle of the Moon             | Zirkel des Mondes            |             |
| Circle of the Shepard          | Zirkel des Hirten            |             |
|                                |                              |             |
| Arcane Archer                  | Arkaner Bogenschütze         |             |
| Battle Master                  | Kampfmeister                 |             |
| Cavalier                       | Kavalier                     |             |
| Champion                       | Champion                     |             |
| Eldritch Knight                | Mysticher Ritter             |             |
| Psi Warrior                    | Psi-Krieger                  |             |
| Rune Knight                    | Runenritter                  |             |
| Samurai                        | Samurai                      |             |
|                                |                              |             |
| Way of Mercy                   | Weg der Bermherzigkeit       |             |
| Way of Shadow                  | Weg des Schattens            |             |
| Way of the Astral Self         | Weg des Astralen Selbst      |             |
| Way of the Drunken Master      | Weg des Betrunkenen Meisters |             |
| Way of the Four Elements       | Weg der Vier Elemente        |             |
| Way of the Kensei              | Weg des Kensei               |             |
| Way of the Long Death          | Weg des Langen Todes         |             |
| Way of the Open Hand           | Weg der offenen Hand         |             |
| Way of the Sun Soul            | Weg der Sonnenseele          |             |
|                                |                              |             |
| Oath of Conquest               | Schwur der Eroberung         |             |
| Oath of Devotion               | Schwur der Hingabe           |             |
| Oath of Glory                  | Schwur des Ruhmes            |             |
| Oath of Redepmtion             | Schwur der Läuterung         |             |
| Oath of Vengeance              | Schwur der Rache             |             |
| Oath of the Ancients           | Schwur der Alten             |             |
| Oath of the Crown              | Schwur der Krone             |             |
| Oath of the Watchers           | Schwur der Wachsamen         |             |
|                                |                              |             |
| Beast Master                   | Herr der Tiere               |             |
| Fey Wanderer                   | Feenwanderer                 |             |
| Gloomstalker                   | Düsterpirscher               |             |
| Horizon Walker                 | Horizontwanderer             |             |
| Hunter                         | Jäger                        |             |
| Monsterslayer                  | Monsterjäger                 |             |
| Swarmkeeper                    | Hüter des Schwarms           |             |
|                                |                              |             |
| Arcane Trickster               | Arkaner Betrüger             |             |
| Assassin                       | Assassine                    |             |
| Inquisitive                    | Ermittler                    |             |
| Phantom                        | Phantom                      |             |
| Scout                          | Späher                       |             |
| Soulknife                      | Seelenmesser                 |             |
| Swashbuckler                   | Draufgänger (?)              |             |
| Thief                          | Dieb                         |             |
|                                |                              |             |
| Aberrant Mind                  | Ungewöhnlicher Verstand      |             |
| Clockwork Soul                 | Uhrwerkseele                 |             |
| Divine Soul                    | Göttliche Seele              |             |
| Draconic Bloodline             | Drachenblutlinie             |             |
| Shadow Magic                   | Schattenmagie                |             |
| Storm Sorceror                 | Strumzauberei                |             |
| Wild Magic                     | Wilde Magie                  |             |
|                                |                              |             |
| The Archfey                    | Die Erzfee                   |             |
| The Celestial                  | Der Himmlische               |             |
| The Fathomless                 | Der Abgründige               |             |
| The Fiend                      | Der Unhold                   |             |
| The Genie                      | Der Dschinn                  |             |
| The Great Old One              | Der Große Alte               |             |
| The Hexblade                   | Die Fluchklinge              |             |
| The Undead                     | Der Untote                   |             |
| The Undying                    | Der Unsterbliche             |             |
|                                |                              |             |
| Bladesinging                   | Klingengesang                |             |
| Order of Scribes               | Orden der Schreiber          |             |
| School of Abjuration           | Schule der Bannmagie         |             |
| School of Conjuration          | Schule der Hervorrufung      |             |
| School of Divination           | Schule der Erkenntnismagie   |             |
| School of Enchantment          | Schule der Verzauberung      |             |
| School of Evocation            | Schule der Beschwörung       |             |
| School of Illusion             | Schule der Illusion          |             |
| School of Necromancy           | Schule der Nekromantie       |             |
| School of Transformation       | Schule der Verwandlung       |             |
| War Magic                      | Kriegsmagie                  |             |
|                                |                              |             |



## Links

- [D3: Dungeons & Dragons auf Deutsch](https://www.dnddeutsch.de/), vor allem
  der [Übersetzer](https://www.dnddeutsch.de/uebersetzer/).
  

