Title: Zeit-Vokabeln
Date: 2023-05-12
Category: german
Tags:
Summary:

Meist sprachliche und schriftliche Wörter sind jeweils als [spr] und [schr] markiert.

# Zeitliche Wörter

## Adverbien in der Vergangenheit

| englisch                   | deutsch                                                    | Erläuterung                            |
|:---------------------------|:-----------------------------------------------------------|:---------------------------------------|
| in the distant past        | in ferner Vergangenheit                                    |                                        |
| once                       | einst, einmal                                              |                                        |
| long ago                   | vor langer Zeit                                            |                                        |
| already                    | schon, bereits, schon längst                               |                                        |
| long since                 | längst                                                     | vor langer Zeit abgeschlossen          |
| in the past                | früher, in der Vergangenheit                               | aber nicht mehr                        |
| for a long time            | lange, seit langem                                         | andauernd                              |
| not for a long time        | längst nicht mehr                                          | wirklich abgeschlossen                 |
| never before               | nie zuvor                                                  |                                        |
| ever before                | je zuvor [spr]                                             |                                        |
| ever                       | schon mal, jemals, schon mal zuvor usw.                    |                                        |
| never                      | noch nicht, noch nie                                       | ohne Absicht                           |
| not yet                    | noch nicht                                                 | mit Absicht                            |
| no longer                  | nicht mehr                                                 |                                        |
| at the time                | damals, zu dieser Zeit, in diesem Moment                   | zu einer Zeit, die schon erwähnt wurde |
| formerly                   | früher [spr], vormals, ehemals                             |                                        |
| a while ago                | vor einiger Zeit, vor einer Weile                          |                                        |
| a year ago                 | vor einem Jahr                                             |                                        |
| last year                  | letztes Jahr                                               |                                        |
| yesterday                  | gestern                                                    |                                        |
| yesterday morning          | gestern Morgen                                             |                                        |
| yesterday evening          | gestern Abend                                              |                                        |
| last night                 | letzte Nacht                                               |                                        |
| early this morning         | heute früh                                                 |                                        |
| this morning               | am Morgen, heute Morgen, heute Vormittag                   |                                        |
| recently                   | neulich [spr], vor kurzem, kürzlich [schr], letztlich, neu |                                        |
| freshly                    | frisch                                                     |                                        |
| until recently             | bis vor kurzem                                             | aber nicht mehr                        |
| until just now             | bis gerade                                                 |                                        |
| only recently              | erst vor kurzem, erst neulich uzw.                         | neu angefangen                         |
| lately                     | in letzter Zeit, kürzlich, spät, neulich                   | andauernd                              |
| as of late                 | neuerdings                                                 |                                        |
| not long ago               | unlängst                                                   |                                        |
| in recent years            | in den letzten Jahren                                      |                                        |
| in recent times            | in letzter Zeit, in jüngster Zeit                          |                                        |
| in the last five minutes   | in den letzten fünf Minuten                                |                                        |
| so far, until now, as yet  | bisher, bislang, bis jetzt                                 |                                        |
| a moment ago               | vor einem Momemnt, vor einem Augenblick                    |                                        |
| just now                   | gerade, gerade eben [spr], soeben [schr]                   |                                        |
| only now                   | erst jetzt, jetzt erst, nun erst, erst nun                 | vor kurzem angefangen                  |
| only just now              | gerade eben                                                |                                        |
| most recently              | zuletzt                                                    |                                        |
| so far, until now, to date | bisher, bis jetzt, bislang [schr]                          |                                        |


## Adverbien in der Gegenwart

| englisch                                 | deutsch                                        | Erläuterung               |
|:-----------------------------------------|:-----------------------------------------------|:--------------------------|
| now, currently                           | jetzt, gerade jetzt, nun, gegenwärtig, aktuell |                           |
| right now                                | gerade                                         |                           |
| at the moment                            | im Moment, momentan, derzeit, zur Zeit         |                           |
| for a while                              | eine Zeitlang, eine Weile                      | vorher und auch andauernd |
| still                                    | immer noch, noch immer                         |                           |
| still not                                | immer noch nicht                               |                           |
| as before                                | nach wie vor                                   |                           |
| today                                    | heute                                          |                           |
| this year                                | dieses Jahr                                    |                           |
| just, just now                           | gerade                                         | im Moment passierend      |
| just now doing it                        | gerade dabei                                   |                           |
| just about to                            | kurz davor                                     |                           |
| for now, for the time being              | für jetzt (gerade), vorest, zeitweilig         |                           |
| temporarily                              | vorübergehend, vorläufig                       |                           |
| until further notice                     | bis auf Weiteres                               |                           |
| for a moment                             | nur kurz                                       |                           |
| only for a bit                           | nur kurz, nur kurzzeitig                       |                           |
| only for a while                         | nur für eine Weile, nur eine Zeit lang         |                           |
| for a while                              | eine Zeit lang, eine Weile                     |                           |
| not yet                                  | noch nicht                                     |                           |
| not anymore                              | nicht mehr                                     |                           |
| during the day (specific)                | am Tag                                         |                           |
| during the day (general), in the daytime | tagsüber                                       |                           |
| all day                                  | den ganzen Tag                                 |                           |
| at night                                 | in der Nacht                                   |                           |
| all night                                | die ganze Nacht                                |                           |
| through the night                        | die Nacht hindurch                             |                           |


## Adverbien in der Zukunft

| englisch         | deutsch                                     | Erläuterung |
|:-----------------|:--------------------------------------------|:------------|
| soon             | gleich, bald                                |             |
| right away       | sofort                                      |             |
| shortly          | kurz, demnächst, demnächst mal              |             |
| in a moment      | in einem Moment                             |             |
| any time now     | jeder Zeit (mit Konjunktiv)                 |             |
| this evening     | heute Abend                                 |             |
| tonight          | heute Nacht                                 |             |
| tomorrow         | morgen                                      |             |
| tomorrow morning | morgen früh                                 |             |
| tomorrow evening | morgen Abend                                |             |
| next year        | nächstes Jahr                               |             |
| later            | später                                      |             |
| at some point    | irgendwann                                  |             |
| someday, one day | eines Tages                                 |             |
| never ever       | nie und nimmer, keinesfalls                 |             |
| in the long run  | auf die Dauer, auf lange Sicht, langfristig |             |
| for a long time  | lange, noch lange, für eine lange Zeit      |             |

langfristig, kurzfristig, mittelfristig


## Adverbien der relativen Zeit, Dauer, oder Geschwindigkeit

| englisch                    | deutsch                                               | Erläuterung                       |
|:----------------------------|:------------------------------------------------------|:----------------------------------|
| at the beginning, initially | anfänglich                                            |                                   |
| at first                    | zunächst                                              | wenn etwas danach verändert wurde |
| before, previously          | zuvor, vorher, vorhin                                 | vor einer anderen Handlung        |
| just before that            | kurz zuvor, kurz davor                                |                                   |
| at the same time            | gleichzeitig, zugleich, zur gleichen Zeit, auf einmal |                                   |
| at the same time as that    | dabei                                                 |                                   |
| eventually                  | schließlich                                           |                                   |
| to begin with               | zuerst                                                |                                   |
| next                        | als nächstes                                          |                                   |
| meanwhile                   | inzwischen, mittlerweile, währenddessen               |                                   |
| and then, afterwards        | und dann, danach, anschließend                        |                                   |
| right after that            | gleich danach, gleich nachher                         |                                   |
| only later                  | erst später, erst nachher                             |                                   |
| the previous day            | am Vortag                                             |                                   |
| the next day                | am nächsten Tag                                       |                                   |
| another day                 | anderntags                                            |                                   |
| finally                     | endlich                                               | hat eine Weile gedauert           |
| in the end                  | am Ende                                               | nachdem, alles passiert ist       |
| ultimately                  | endgültig, letzendlich [spr], letzten Endes [schr]    |                                   |
| suddenly                    | plötzlich                                             |                                   |
| all at once                 | auf einmal                                            |                                   |
| abruptly                    | abrupt, schlagartig                                   |                                   |
| gradually                   | allmählich                                            |                                   |
| little by little            | nach und nach                                         |                                   |
| step by step                | Schritt für Schritt, schrittweise                     |                                   |


## Adverbien der Frequenz

| englisch                        | deutsch                                | Erläuterung        |
|:--------------------------------|:---------------------------------------|:-------------------|
| never                           | nie, niemals                           |                    |
| not at all                      | gar nicht                              |                    |
| hardly ever                     | kaum                                   |                    |
| rarely, seldom                  | selten                                 |                    |
| occasionally                    | gelegentlich, zwischendurch, mitunter  |                    |
| now and then, from time to time | hin und wieder, ab und zu              |                    |
| on and off, intermittently      | (immer) zwischendruch                  |                    |
| sometimes                       | manchmal                               |                    |
| at times                        | zuweilen                               | etwas überraschend |
| often                           | oft                                    |                    |
| quite often                     | ganz oft, recht oft                    |                    |
| in many cases                   | in vielen Fällen, oftmals              |                    |
| frequently                      | häufig                                 |                    |
| mostly                          | meistens                               |                    |
| most of the time                | die meiste Zeit                        |                    |
| at any time                     | jederzeit                              |                    |
| always, all the time            | immer                                  |                    |
| constantly                      | stets                                  |                    |
| continually                     | ständig, durchgehend                   |                    |
| once                            | mal, einmal                            |                    |
| many times                      | mehrmals                               |                    |
| [once] again                    | nochmal, noch einmal, erneut, abermals |                    |
| yet again                       | schon wieder                           | mehrmals passiert  |
| never again                     | nie wieder, nie mehr                   |                    |

