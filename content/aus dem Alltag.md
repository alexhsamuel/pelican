| englisch                                  | deutsch                                    | Erläuterung                           |
|:------------------------------------------|:-------------------------------------------|:--------------------------------------|
| that's more like it                       | das kommt eher hin                         |                                       |
| let's just leave that (alone)             | das lassen wir einfach mal (stehen)        |                                       |
| to drop in for a moment                   | auf einen Sprung vorbei-/hereinkommen      | eine Person kurz zu besuchen          |
| to be sick of sb                          | von jdm krank sein                         | gemein, etwas kontinuierlich          |
| off the top of my head                    | so aus dem Kopf                            |                                       |
| to be running late                        | spät dran sein                             |                                       |
| to be too late                            | zu spät dran sein                          |                                       |
| auf die Schnelle                          | in a rush                                  | eilig                                 |
| I'll be there.                            | Ich bin dabei.                             |                                       |
| I'll sub / stand in for you.              | Ich vertrete dich.                         |                                       |
| I better go make dinner.                  | Ich gehe besser, das Abendessen zu machen. | seltener auf Deutsch als auf Englisch |
| shut him/her up                           | stell ihn/sie ruhig                        |                                       |
| to sit around                             | rumsitzen                                  |                                       |
| to play around                            | rumspielen                                 |                                       |
| quick, smart                              | fix                                        |                                       |
| run down, beat, knackered                 | fix und fertig                             |                                       |
| Why did I come here?                      | Warum bin ich hierher gekommen?            |                                       |
| anyone else would …                       | jeder andere würde …                       |                                       |
| Now nothing will stop us.                 | Jetzt hält uns nichts mehr auf.            |                                       |
| put sth in the trash can                  | etw in den Mülleimer tun                   |                                       |
| Couldn't I have come up with that myself? | Hätte ich da nicht draufkommen können?     |                                       |
| I couldn't do that before, either.        | Das konnte ich vorher auch schon nicht.    |                                       |
| They'll lock me up!                       | Die sperren mich ein!                      |                                       |
| Is there a discount?                      | Gibt's Nachlass?                           |                                       |
| It's on me.                               | Geht auf mich.                             | beim Bezahlen / Abrechnen             |
| here we go                                | dann mal los                               |                                       |
| false alarm                               | falscher Alarm                             |                                       |
| be difficult for sb to do sth             | es jdm schwerfallen, etw zu tun            |                                       |
| to talk about sth                         | von etw reden / sprechen                   |                                       |
| Help yourself.                            | Bedien dich.                               |                                       |
| soaking wet                               | klatschnass                                |                                       |
| to make fun of sb                         | jdn aufziehen                              | ständig                               |
| to be made fun of                         | aufgezogen werden                          |                                       |
| to look stupidly                          | blöd schauen                               |                                       |
| go nuts, go ballistic, crack              | durchdrehen                                |                                       |
| to come for sb                            | auf jdn auskommen                          |                                       |
| we'll settle it between us                | wir machen das unter uns aus               |                                       |


## Gesprächsfluss

| englisch                                      | deutsch                       | Erläuterung  |
|:----------------------------------------------|:------------------------------|:-------------|
| let's see …                                   | mal sehen …                   |              |
| listen, …                                     | hör zu, …                     |              |
| ya' know, …                                   | weißt du / wissen Sie, …      |              |
| to be honest, …                               | mal ehrlich …                 |              |
| let me be blunt                               | ich sag's ganz unverblümt     |              |
| Now I understand.                             | Dann weiß ich Bescheid.       |              |
| I shouldn't delude myself                     | ich darf mir nichts vormachen |              |
| would be to good to be true                   | wär ja auch zu schön          |              |
| I'll be damned!                               | Meine Fresse!                 |              |
| Is that possible?                             | Geht so was?                  |              |
| that would surprise me                        | das würde mich wundern        |              |
| that can't be true                            | das darf doch nicht wahr sein |              |
| doesn't say anything to me                    | sagt mir gar nichts           |              |
| let's say, …                                  | sagen wir mal, …              |              |
| angenommen, …                                 | suppose …                     | hypothetisch |
| not even                                      | nicht mal                     |              |
| basically, essentially                        | im Wesentlichen               |              |
| all over again                                | ganz von vorn                 |              |
| How was that again?                           | Wie war das gleich?           |              |
| if possible                                   | wenn es geht                  |              |
| as usual, as before                           | wie gehabt                    |              |
| that seems / feels like …                     | das kommt einem vor wie …     |              |
| now that you mention it                       | jetzt, wo du es erwähnst      |              |
| while we're at it, while we're on the subject | wenn wir schon mal dabei sind |              |
| one more thing                                | noch eins                     |              |
| I quote, …                                    | ich zitiere, …                |              |


## Alles in Ordnung?

| englisch                                     | deutsch                                      | Erläuterung                         |
|:---------------------------------------------|:---------------------------------------------|:------------------------------------|
| Are you ok?                                  | Geht's?  Kommst du klar?                     |                                     |
| What are you up to?                          | Was treibst du denn?                         |                                     |
| What's wrong with me?                        | Was ist los mit mir?                         |                                     |
| to be worried                                | sich (+Dat) Sorgen machen                    |                                     |
| to be scared                                 | Schiss haben                                 |                                     |
| Everything will be OK.                       | Alles wird gut.                              |                                     |
| Don't panic.                                 | Keine Panik.                                 |                                     |
| Take it easy.                                | Nur die Ruhe.                                | formell                             |
| It's going to be OK.                         | Das wird wieder.                             |                                     |
| Nothing to fear!                             | Keine Bange!                                 |                                     |
| nothing to complain about                    | an etw/jdm nichts auszusetzen                |                                     |
| come on, now                                 | komm schon                                   |                                     |
| that can't be so hard                        | das kann ja nicht weiter schwer sein         |                                     |
| keep still already                           | halt doch endlich still                      |                                     |
| You're not going to get sick on me, are you? | Du wirst mir doch nicht krank werden?        | ugs., über etwas ungewollt          |
| I have arranged it / settled it.             | Ich habe es geregelt.                        |                                     |
| That's enough!                               | Das reicht jetzt!                            |                                     |
| That was close.                              | Das war knapp.                               |                                     |
| Don't be like that.                          | Hab dich nicht so. / Stell dich nicht so an. |                                     |
| Don't be angry with me.                      | Sei mir nicht böse.                          |                                     |
| Don't be mad.                                | Sei doch nicht sauer.                        |                                     |
| I'll make it up to you.                      | Ich mach es wieder gut.                      |                                     |
| no problem                                   | keine Frage                                  | aber nicht im Sinn von _bitteschön_ |
| no problem (you're welcome)                  | nichts dafür / kein Ding / keine Ursache     |                                     |
| not worth mentioning ; it's nothing          | nicht der Rede wert                          |                                     |
| We could do with that.                       | Wir können es gebrauchen.                    |                                     |
| We could do without that.                    | Wir können es nicht gebrauchen.              |                                     |
| What came of it?                             | Was kam dabei raus?                          |                                     |
| It'll do you good.                           | Wird dir guttun.                             |                                     |
| great!                                       | großartig!                                   |                                     |
| so lucky!                                    | so ein Glück!                                |                                     |

