Title: Rollenspiel-Vokabeln
Date: 2023-02-05
Category: german rpg
Tags: dev
Summary: Übersetzung von Rollenspiel-Vokabeln ins Deutsche

## Waffen

| englisch      | deutsch                   | Erläuterung |
|:--------------|:--------------------------|:------------|
| dagger        | der Dolch (pl –n)         |             |
| club          | der Knüppel (pl –n)       |             |
| quarterstaff  | der Kampfstab (pl –̈e)     |             |
| spear         | der Speer (pl –e)         |             |
| mace          | der Streitkolben (pl –)   |             |
| bow           | der Bogen (pl –̈)          |             |
| arrow         | der Pfeil (pl –e)         |             |
| crossbow      | die Armbrust (pl –̈e)      |             |
| crossbow bolt | der Armbrustbolzen (pl –) |             |
| projectile    | das Geschoss (pl –e)      |             |
| trident       | der Dreizack (pl –e)      |             |
| battleaxe     | die Streitaxt (pl –̈e)     |             |
| sword         | das Schwert (pl –er)      |             |
| sabre         | der Säbel (pl –)          |             |
| scimitar      | der Krummsäbel (pl –)     |             |

## Rüstung

| englisch    | deutsch            | Erläuterung |
|:------------|:-------------------|:------------|
| scale mail  | der Schuppenpanzer |             |
| chain mail  | der Kettenpanzer   |             |
| splint mail | der Schienenpanzer |             |
| plate mail  | der Plattenpanzer  |             |
| shield      | das Schild (pl –e) |             |

## Ausrüstung

| englisch       | deutsch                     | Erläuterung |
|:---------------|:----------------------------|:------------|
| crowbar        | die Breschstange (pl –n)    |             |
| torch          | die Fackel (pl –n)          |             |
| healing potion | der Heiltrank (pl –̈e)       |             |
| rope           | das Seil (pl –e)            |             |
| grappling hook | der Kletterhaken (pl –)     |             |
| sledgehammer   | der Vorschlaghammer (pl –̈e) |             |
| whetstone      | der Setzstein (pl –e)       |             |
| tinderbox      | das Zunderkästchen (pl –)   |             |

## Gelände

| englisch             | deutsch                                     | Erläuterung |
|:---------------------|:--------------------------------------------|:------------|
| wilderness           | die Wildnis                                 |             |
| wasteland            | das Ödland                                  |             |
| forest               | der Wald (pl –̈er)                           |             |
| foliage              | das Laub                                    |             |
| forest canopy        | das Laubdach (pl –̈er)                       |             |
| clearing             | die Lichtung (pl –en)                       |             |
| grove                | der Hain (pl –e)                            |             |
| jungle               | der Dschungel (pl –)                        |             |
| grassland            | die Wiesen                                  |             |
| plains               | die Ebene (pl –n)                           |             |
| desert               | die Wüste (pl –n)                           |             |
| dune                 | die Düne (pl –n)                            |             |
| oasis                | die Oase (pl –n)                            |             |
| tundra               | die Tundra (pl –)                           |             |
| glacier              | der Gletscher (pl –)                        |             |
| marsh                | der Sumpf (pl –̈e)                           |             |
| swamp                | der Sumpf (pl –̈e)                           |             |
| moor, bog            | das Moor (pl –e)                            |             |
| quicksand            | der Treibsand                               |             |
| hill                 | der Hügel (pl –)                            |             |
| mountains            | das Gebirge                                 |             |
| mountain pass        | der Bergpass (pl –̈e)                        |             |
| peak, summit         | die (Berg)Spitze (pl –n), der Gipfel (pl –) |             |
| valley               | das Tal (pl –̈er)                            |             |
| gorge, canyon        | die Schluct (pl –en)                        |             |
| slope                | der Abhang (pl –̈e)                          |             |
| outcropping          | der Felsvorsprung (pl –e)                   |             |
| ridge                | der (Berg)Kamm (pl –̈e)                      |             |
| hilltop, knoll       | die Kuppe (pl –n)                           |             |
| cave                 | die Höhle (pl –n)                           |             |
| mine                 | die Mine (pl –n)                            |             |
| quarry               | der Steinbruch (pl –̈e)                      |             |
| lake                 | der See (pl –n)                             |             |
| pond                 | der Teich (pl –e), der Weiher (pl –)        |             |
| river                | der Fluss (pl –̈e)                           |             |
| stream, creek, brook | der Bach (pl –̈e)                            |             |
| waterfall            | der Wasserfall (pl –̈e)                      |             |
| dam                  | der Damm (pl –̈e)                            |             |
| ocean                | das Meer (pl –e)                            |             |
| bay                  | die Bucht (pl –en)                          |             |
| shore                | das Ufer (pl –)                             |             |
| coast                | die Küste (pl –n)                           |             |
| town                 | die Ortschaft (pl –en)                      |             |
| village              | das Dorf (pl –̈er)                           |             |
| farm                 | der Bauernhof (pl –̈e)                       |             |
| field                | das Feld (pl –er)                           |             |
| meadow               | die Wiese (pl –n)                           |             |
| camp                 | das Lager (pl –)                            |             |
| battlefield          | das Schlachtfeld (pl –er)                   |             |
| hilly                | hügelig                                     |             |
| steep                | steil                                       |             |
| flat                 | flach                                       |             |
| impassible           | unpassierbar                                |             |
| treacherous          | tückisch                                    |             |
| choppy               | unruhig                                     |             |
| forested             | bewaldet                                    |             |
| sylvan               | waldig                                      |             |
| remote               | abgelegen                                   |             |
| abandoned            | verlassen, aufgegeben                       |             |
| arduous journey      | die beschwerliche Reise                     |             |
| high altitude        | große Höhe                                  |             |

## Sonstig

| englisch       | deutsch                 | Erläuterung |
|:---------------|:------------------------|:------------|
| treasure chest | die Schatzkiste (pl –n) |             |
|                |                         |             |
