Title: Einladungen
Date: 2023-09-19
Category: german
Tags:
Summary:

Was man sagt beim Einladen und bei gesellschaftlichen Events.

| englisch                             | deutsch                                                               | Erläuterung                                          |
|:-------------------------------------|:----------------------------------------------------------------------|:-----------------------------------------------------|
| I'd like to invite you.              | Ich würde dich gerne einladen. / Ich lad dich ein.                    |                                                      |
| I'd like to invite you and a +1.     | Ich lad dich ein (+1).                                                |                                                      |
| Who is going to host?                | Bei wem machen wir das?                                               |                                                      |
|                                      | Zu wem könnten wir gehen?                                             |                                                      |
| Do you want to come over?            | Möchtet ihr zu mir kommen?                                            |                                                      |
| Would you like to come for dinner?   | Ich würde euch gerne zum Abendessen einladen.                         | Abendessen zu Hause.                                 |
|                                      | Ich würde euch gerne zum Essen einladen.                              | Nicht unbedingt zu Hause; könnte im Restaurant sein. |
| Come over, if you're free.           | Wenn du noch keine Pläne hast, komm doch vorbei.                      |                                                      |
| Come over, if you feel like it.      | Falls du Lust hast, komm doch vorbei.                                 |                                                      |
| Will there be dinner?                | Gibt es etwas zum Essen?                                              |                                                      |
| What's for dinner?                   | Was hast du geplant zu kochen?                                        |                                                      |
|                                      | Was hast du vor zu kochen?                                            |                                                      |
| What should I wear?                  | Was zieht man da an?                                                  |                                                      |
| Is there a dress code?               | Gibt es einen Dresscode?                                              |                                                      |
| RSVP                                 | Gib / Gebt bitte kurz Bescheid.                                       |                                                      |
| RSVP by Sept. 9                      | Gibt mir bitt bis zum 9. September Bescheid, ob ihr kommt oder nicht. | formeller                                            |
| BYOB                                 | Bitte Getränke selber mitbringen.                                     |                                                      |
| Please invite your friends.          | Bring gerne deine Freunde mit.                                        |                                                      |
| Text me when you get here.           | Schick mir eine SMS, wenn du da bist.                                 |                                                      |
| Call me when you get here.           | Ruf kurz durch, wenn du da bist.                                      |                                                      |
|                                      | Klingel kurz durch, wenn du da bist.                                  |                                                      |
| Thanks for inviting me.              | Danke für die Einladung.                                              |                                                      |
| Can I bring anything?                | Kann ich etwas mitbringen?                                            |                                                      |
| Is there anything you don't eat?     | Gibt es etwas, was du nicht ißt / essen darfst / essen kanst?         |                                                      |
| I don't think I can make it.         | Ich denke, ich schaff(e) es nicht.                                    |                                                      |
| I already have something.            | Ich hab(e) leider schon etwas vor.                                    |                                                      |
| Something came up.                   | Mir ist etwas dazwischen gekommen.                                    |                                                      |
| Come in.                             | Kommt rein.                                                           |                                                      |
| Throw your coats there / on the bed. | Die Jacken könnt ihr da / aufs Bett legen.                            |                                                      |
| No need to take your shoes off.      | Ihr könnt die Schuhe ruhig anlassen.                                  | Ruhig bedeutet etwas wie, keine Sorgen.              |
| I'll treat you.                      | Ich lad(e) dich ein.                                                  |                                                      |
| It's on me.                          | Geht auf mich.                                                        | Nachdem etwas bestellt worden ist.                   |
| I'm taking off.                      | Ich geh los.                                                          |                                                      |
|                                      | Ich glaube, ich geh mal.                                              |                                                      |
|                                      | Ich mach mich los.                                                    | Berlin / Ostdeutschland                              |
|                                      | Ich glaube, wir machen los, oder?                                     |                                                      |
| Thanks for coming.                   | Schön, das du da warst / das du da gewesen bist.                      |                                                      |
| Safe trip home.                      | Komm gut nach Hause.                                                  |                                                      |
| I had a nice time.                   | Ich hatte ein gute Zeit.                                              |                                                      |
|                                      | Es war sehr schön bei dir.                                            |                                                      |
| I had a nice evening.                | Ich hatte einen schönen Abend.                                        |                                                      |
| Can I Venmo you?                     | Was kann ich dir venmoen?                                             | :P                                                   |
| What do I owe you for the beers?     | Was schuld ich dir für die Bier?                                      |                                                      |
|                                      | Was bekommst du von mir für die Bier?                                 |                                                      |

